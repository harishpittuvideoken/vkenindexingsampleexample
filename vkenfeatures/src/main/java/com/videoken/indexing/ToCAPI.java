package com.videoken.indexing;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

public class ToCAPI extends AsyncTask<String, Void, ToCAPI.TocResult> {

    private static String TOC_URL = "https://apiv3.videoken.com/videoken-api/api/v2/mmtocnew?youtube_id=";
//    private static String TOC_URL = "https://apiv3.videoken.com/videoken-api/api/v2/mmtocnew?youtube_id=vdocipher-";

    private ToCAPIListener toCAPIListener;

    private static String TAG = "ToCAPI";

    public ToCAPI(ToCAPIListener toCAPIListener) {
        this.toCAPIListener = toCAPIListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        toCAPIListener.onTocStarted();
    }

    @Override
    protected TocResult doInBackground(String... urls) {

//        Log.d("TocAPI", "doInBackground: "+urls[0] );

        TocResult tocResult = new TocResult();
        String videoId = urls[0];
        ArrayList<ToCObject> tocList = new ArrayList<>();

        URL githubEndpoint = null;
        try {
            githubEndpoint = new URL(TOC_URL + videoId);

            HttpsURLConnection myConnection =
                    (HttpsURLConnection) githubEndpoint.openConnection();

            InputStream responseBody = myConnection.getInputStream();
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(responseBody, "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();

            String inputStr;
            while ((inputStr = streamReader.readLine()) != null)
                responseStrBuilder.append(inputStr);

            JSONObject jsonObject = new JSONObject(responseStrBuilder.toString());

            Log.i(TAG, "responseStrBuilder:" + responseStrBuilder);
            Log.i(TAG, "responseStrBuilder:" + jsonObject.has("error") );

            if (jsonObject.has("error") || (jsonObject.has("status") && !jsonObject.getString("status").equalsIgnoreCase("processed"))) {
                tocResult.setMessage("Table of Contents is not available for this video. A request to generate Table Of Contents has been submitted.");
            }

            JSONObject tocJson = jsonObject.getJSONObject("mmtoc");

            /*toc parseing start*/

            Iterator<String> iterator = tocJson.keys();
            while (iterator.hasNext()) {
                String key = iterator.next();
                ToCObject tocObject = new ToCObject(Integer.parseInt(key), tocJson.optString(key));
                tocList.add(tocObject);
                Log.i(TAG, "key:" + key + "--Value::" + tocJson.optString(key));
            }
            /*toc parsing end*/

            if(tocList.size()>0) {
                tocResult.setTocList(tocList);
            }else{
                tocResult.setMessage("No Table of Contents");
            }
        } catch (MalformedURLException e) {
            Log.i(TAG, "MalformedURLException:" + e.getMessage());

            e.printStackTrace();
        } catch (IOException e) {
            Log.i(TAG, "IOException:" + e.getMessage());

            e.printStackTrace();
        } catch (JSONException e) {
            Log.i(TAG, "JSONException:" + e.getMessage());

            e.printStackTrace();
        }
        Collections.sort(tocList);

        return tocResult;
    }

    protected void onPostExecute(TocResult tocResult) {
        Log.e(TAG, "onPostExecute: " + tocResult.getMessage());
        super.onPostExecute(tocResult);
        if (tocResult.getMessage() != null) {
            toCAPIListener.onTocFailure(tocResult.getMessage());
        } else {
            toCAPIListener.onTocSuccess(tocResult.getTocList());
        }
    }

    public interface ToCAPIListener {
        void onTocStarted();

        void onTocSuccess(ArrayList<ToCObject> tocList);

        void onTocFailure(String message);
    }

    class TocResult {
        ArrayList<ToCObject> tocList;
        String message;

        public ArrayList<ToCObject> getTocList() {
            return tocList;
        }

        public void setTocList(ArrayList<ToCObject> tocList) {
            this.tocList = tocList;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

}



