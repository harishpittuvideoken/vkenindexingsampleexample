package com.videoken.indexing.utils.custom_views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by harishpittu on 14/09/17.
 */

public class QuicksandLightTextView extends TextView {

    private Typeface typeface;
    public QuicksandLightTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public QuicksandLightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public QuicksandLightTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        if(typeface==null) {
             typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/QuicksandLight.ttf");
            setTypeface(typeface);
        }
    }

}
