package com.videoken.indexing;

public class ToCObject implements Comparable<ToCObject> {
    private int duration ;
    private String title;
    private boolean highligted = false;

    public ToCObject(int duration, String title) {
        this.duration = duration;
        this.title = title;
    }

    public boolean isHighligted() {
        return highligted;
    }

    public void setHighligted(boolean highligted) {
        this.highligted = highligted;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int compareTo(ToCObject toCObject) {
        return (this.getDuration() < toCObject.getDuration() ? -1 : (this.getDuration() == toCObject.getDuration() ? 0 : 1));
    }
}
